package com.company;

import java.util.stream.Stream;

public class Question{
    private String questionString;
    private String[] options;
    private int cost;
    private int[] userOption;
    private int[] correctOption;

    public Question(String questionString, int optionsCount, int cost){
        this.questionString = questionString;
        this.options = new String[optionsCount];
        this.cost  = cost;
    }

    public void updateOption(int optionsPosition, String optionsString){
        this.options[optionsPosition] = (optionsPosition+1)+") "+optionsString;
    }
    public void setCorrectOption(String correctOption){
        this.correctOption = Stream.of(correctOption.split(" ")).mapToInt(Integer::parseInt).toArray();
    }

    public String getQuestionString() {
        return questionString;
    }
    public boolean isMultiChoice() {
        return correctOption.length>1;
    }
    public int[] getCorrectOption() {
        return correctOption;
    }
    public String getFormattedOptions(){
        return String.join("\n",this.options);
    }
    public int getCost(){
        return cost;
    }
    public int[] getUserOption(){
        return userOption;
    }
    public void setUserOption(String option){
        userOption = Stream.of(option.split(" ")).mapToInt(Integer::parseInt).toArray();
    }
}
