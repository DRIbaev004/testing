package com.company;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    static Scanner sc;
    static ArrayList<Question> questions;
    static double score;
    static ArrayList<Question> failedQuestions;

    public static void getQuestions() {
        while (true) {
            System.out.println("Введите новый вопрос или напишите 'q/Q', чтобы закончить: ");
            String question = sc.nextLine();
            if (question.trim().toLowerCase().equals("q")) {
                break;
            }
            System.out.print("Введите количество ответов: ");
            int optionsCount = sc.nextInt();
            sc.nextLine();
            System.out.print("Введите количество баллов за правильный ответ: ");
            int cost = sc.nextInt();
            sc.nextLine();
            Question newQuestion = new Question(question, optionsCount,cost);
            for (int i = 0; i < optionsCount; i++) {
                System.out.print((i + 1) + ")");
                String option = sc.nextLine();
                newQuestion.updateOption(i, option);
            }
            System.out.println("Введите индекс правильного овтета (напишите ответы с пробелем, если их несколько)");
            newQuestion.setCorrectOption(sc.nextLine());
            questions.add(newQuestion);
            System.out.println("----------------Добавил!----------------\n");
        }
    }

    public static void displayQuestionAndGetAnswer(Question question){
        System.out.println(question.getQuestionString()+" ("+question.getCost()+" балла) "+(question.isMultiChoice()?" [несколько правильных ответов]":""));
        System.out.println(question.getFormattedOptions());
        String userAnswer = sc.nextLine();
        if(question.isMultiChoice()){
            double sc = gradeMultiChoice(userAnswer,question.getCorrectOption(),question.getCost());
            score+=sc;
            if(sc != question.getCost()){
                question.setUserOption(userAnswer);
                failedQuestions.add(question);
            }
        }else {
            if (Integer.parseInt(userAnswer)==question.getCorrectOption()[0]) {
                score += question.getCost();
            } else {
                question.setUserOption(userAnswer);
                failedQuestions.add(question);
            }
        }
    }
    public static void startTest() {
        score = 0;
        failedQuestions = new ArrayList<>();
        int testQuestionsCount;
        do {
            System.out.print("Введите сколько вопросов будет в тесте:");
            testQuestionsCount = sc.nextInt();
            sc.nextLine();
        } while (testQuestionsCount > questions.size());

        ArrayList<Question> shuffledQuestions = new ArrayList<>(questions);
        Collections.shuffle(shuffledQuestions);
        int totalScore = getTotalScore(shuffledQuestions, testQuestionsCount);

        Date start = new Date();
        for (int i = 0; i < testQuestionsCount; i++) {
            displayQuestionAndGetAnswer(shuffledQuestions.get(i));
        }
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date finish = new Date();
        String timeString = timeFormat.format(new Date(finish.getTime()-start.getTime()));

        System.out.println("Ваши баллы "+score+"/"+totalScore);
        System.out.println("Вы закончили тест за "+timeString);
        System.out.println("Ошибки: \n");
        for(Question question: failedQuestions){
            System.out.println(question.getQuestionString()+ "("+question.getCost()+" балла)");
            System.out.println(question.getFormattedOptions());
            System.out.println("Ваш вариант: "+ Arrays.toString(question.getUserOption()));
            System.out.println("Правильный вариант: "+ Arrays.toString(question.getCorrectOption()));
            System.out.println();
        }
    }

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        questions = new ArrayList<>();
        getQuestions();
        mainLoop:
        while (true) {
            System.out.println("Напишите '/add', чтобы добавить вопрос " +
                    "\n Напишите '/test', чтобы начать тест " +
                    "\n Напишите '/exit', чтобы завершить тест");
            String text = sc.nextLine();
            switch (text) {
                case "/test":
                    startTest();
                    break;
                case "/add":
                    getQuestions();
                    break;
                case "/exit":
                    break mainLoop;
                default:
                    break;
            }
        }
    }
    public static int getTotalScore(ArrayList<Question> questions, int questionCount){
        int totalScore = 0;
        for (int i = 0; i < questionCount; i++) {
           totalScore+= questions.get(i).getCost();
        }
        return totalScore;
    }
    public static double gradeMultiChoice(String userChoice, int[] correctAnswer,int questionScore){
        HashSet<Integer> answerSet = Arrays.stream(correctAnswer)
                .boxed()
                .collect(Collectors.toCollection(HashSet::new));
        double currentScore = 0;
        double unitScore = (double)questionScore/correctAnswer.length;
        String[] userChoices = userChoice.split(" ");
        for(String x: userChoices){
            int xToInt = Integer.parseInt(x);
            if(answerSet.contains(xToInt)){
                currentScore+=unitScore;
                answerSet.remove(xToInt);
            }else{
                currentScore-=unitScore;
            }
        }
        currentScore = Math.max(0, currentScore);
        return currentScore;
    }
}
